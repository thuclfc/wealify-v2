$(document).ready(function () {
    //show menu
    $('.navbar-toggle').on('click', function () {
        $(this).toggleClass('active');
        $('.navbar-collapse').toggleClass('show');
    });
    $('.navbar-collapse .close').on('click', function () {
        $('.navbar-collapse').removeClass('show');
    });
// active navbar of page current
    var urlcurrent = window.location.pathname;
    $(".navbar-nav li a[href$='"+urlcurrent+"'],.faq-list ul li a[href$='"+urlcurrent+"']").addClass('active');

    $(window).on("load", function () {
        $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'></span>");
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('header').addClass('scroll');
        }else{
            $('header').removeClass('scroll');
        }
    });

    $('.feedback-list').slick({
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        arrows: true,
    });


    //accordion
    var accordion_content = $('.faq-list .item__content');
    var accordion__header        = $('.faq-list .item__header');
    var accordion_active = $('.faq-list > .active');
    accordion_content.hide();
    accordion_active.find('.item__content').slideUp();

    accordion__header.click(function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('active');

        if (accordion_content.parent('.active')) {
            $(this).parent().find('.item__content').slideToggle();
        }
        return false;
    });

    $('.faq-list .tab li a').on('click',function (){
        $('.faq-list .tab li a').removeClass('active');
    });
});

